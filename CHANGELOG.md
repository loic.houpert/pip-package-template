# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.3.1](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/1.3.0...1.3.1) (2023-04-24)


### Bug Fixes

* bug documentation duplicate class methods ([f55227e](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/f55227ea8e0dacd1c8dfec96479282328b60f08b))
* move licence and ci in separate files ([36901d9](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/36901d944c2c7c169b8004d2336ec011a0bdb63a))

## [1.3.0](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/1.2.2...1.3.0) (2023-03-13)


### Features

* add config files and instructions for using pre-commit ([c8ded65](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/c8ded6510e1583d478dddc9daee85d8097011e95))

## [1.2.2](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/1.2.1...1.2.2) (2023-03-08)


### Bug Fixes

* cicd rules ([5d4cb5d](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/5d4cb5d9327db9f258d605b047c33f691a6df069))

## [1.2.1](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/1.2.0...1.2.1) (2023-02-24)


### Features

* add dev info readme ([467e2cb](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/467e2cb807e9248bc7651ce7f337fdcaba2a1b06))

## [1.2.0](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/1.1.0...1.2.0) (2023-02-24)


### Features

* add auto-tag-merge function to gitlab ci ([05ee933](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/05ee933c01c4033ab24449e6dc24ab8360b80704))
* add python packages ([a444629](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/a4446299bf57dadf669693254e77617af46323ed))

## [1.1.0](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.7...1.1.0) (2023-02-17)


### Features

* add ci template variable ([766e329](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/766e329b8bcb520cd074db5490d594233f68b330))

## [1.0.0](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.7...1.0.0) (2023-02-17)

## [0.1.7](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.6...0.1.7) (2023-02-15)


### Bug Fixes

* container version ([1459df0](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/1459df00a7981bf14f3065a7f18ef802dabf181c))
* only publish package when commit or merge request is made on default branch ([308b379](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/308b3791922290bccf99fcdd88b31528746f7eec))

## [0.1.6](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.5...0.1.6) (2023-02-10)


### Features

* add setuptools_scm ([7623463](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/76234634b18d524637fe6b1909d7d95ebd283883))
* publish package in gitlab registry ([7d7d9ae](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/7d7d9ae4e52eeeb3b0e1cf167dfa1a7c6910d6cb))


### Bug Fixes

* add tests directory ([f487eec](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/f487eec71dbc753c41245753fde789a1c89b0f0d))

## [0.1.5](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.4...0.1.5) (2023-02-08)


### Bug Fixes

* bash -> sh ([8853d95](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/8853d95cb61c3aabedceaec938dffdb0d167b54e))
* formatting conflict ([5bc9a37](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/5bc9a371ae52cf1638d3a3cd961a0d12058750fe))

## [0.1.4](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.2...0.1.4) (2023-02-07)


### Features

* add ci/cd ([6071a8a](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/6071a8a267b48ab504526c86ea3bab339c2d5adb))
* follow last standard for packaging python software ([410d599](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/410d5995809518789dff234afcde92421d65c5b2))


### Bug Fixes

* add details ([cc0fa14](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/cc0fa1407a9b91f3f3508bae9538784e1358000e))
* formatting ([ddfdc44](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/ddfdc446db54bfc9e6c0c7c4b74ada9efa78ab32))
* formatting rules ([9398c5e](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/9398c5e985d6fb063bd10d8f58e04c2f1dac7f6b))

## [0.1.3](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.2...0.1.3) (2023-02-07)


### Features

* add ci/cd ([6071a8a](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/6071a8a267b48ab504526c86ea3bab339c2d5adb))
* follow last standard for packaging python software ([410d599](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/410d5995809518789dff234afcde92421d65c5b2))


### Bug Fixes

* add details ([cc0fa14](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/cc0fa1407a9b91f3f3508bae9538784e1358000e))
* formatting ([ddfdc44](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/ddfdc446db54bfc9e6c0c7c4b74ada9efa78ab32))
* formatting rules ([9398c5e](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/9398c5e985d6fb063bd10d8f58e04c2f1dac7f6b))

## [0.1.2](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/compare/0.1.1...0.1.2) (2023-01-27)

## 0.1.1 (2023-01-27)


### Features

* add template ([fc21b70](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package/commit/fc21b709d2626f2d8bb19fd2036cad23537541d7))
