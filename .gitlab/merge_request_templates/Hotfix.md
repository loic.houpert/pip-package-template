## Hotfix Description

<!-- Describe briefly what the purpose of the hotfix is. -->

## Author Checklist

- [ ] Your [responsibilities as an author](https://dwpdigital.atlassian.net/wiki/spaces/IASC/pages/70705764195/Code+Review#CodeReview-Theresponsibilityoftheauthor) are **understood** and have been adhered to.
- [ ] [Technical principles](https://engineering.dwp.gov.uk/policies/technical-principles/) have been followed.
- [ ] [Coding standards](https://dwpdigital.atlassian.net/wiki/spaces/IASC/pages/70717637748/Coding+Standards) have been followed.
- [ ] Sensitive data is retrieved at run-time using a secrets management tool (AWS Parameter Store, etc).
- [ ] No sensitive data is hard-coded.
- [ ] No sensitive data is being logged.
- [ ] Related documentation has been created or updated.
- [ ] Related unit tests have been created or updated, and include both positive and negative tests that cover all acceptance criteria.
- [ ] **All** project unit tests pass.
- [ ] Code quality checks pass.
- [ ] CI/CD pipeline stages pass.
- [ ] Merge request target branch is **develop**, **master**, **main** or an active **release**.
- [ ] No merge conflicts exist.

## Reviewer Checklist

- [ ] Assign the merge request to yourself, to let others know you are reviewing it. Drop everything else, and review the merge request as soon as you are assigned to it. If you cannot perform the review within the [Review-response SLO](https://dwpdigital.atlassian.net/wiki/spaces/IASC/pages/70705764195/Code+Review#CodeReview-Review-responseSLO) time frame, let the author know as soon as possible.
- [ ] Call the author as soon as possible to discuss the hotfix so it can be fully understood.
- [ ] Your [responsibilities as a reviewer](https://dwpdigital.atlassian.net/wiki/spaces/IASC/pages/70705764195/Code+Review#CodeReview-Theresponsibilityofthereviewer) are **understood** and have been adhered to.
- [ ] [Technical principles](https://engineering.dwp.gov.uk/policies/technical-principles/) have been followed.
- [ ] [Coding standards](https://dwpdigital.atlassian.net/wiki/spaces/IASC/pages/70717637748/Coding+Standards) have been followed.
- [ ] Sensitive data is retrieved at run-time using a secrets management tool (AWS Parameter Store, etc).
- [ ] No sensitive data is hard-coded.
- [ ] No sensitive data is being logged.
