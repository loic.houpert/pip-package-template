# Cookiecutter Template for Python package

_A logical, reasonably standardized, but flexible project structure for developing python package._

## Requirements to use the cookiecutter template

- Python 3.5+
- [Cookiecutter Python package](http://cookiecutter.readthedocs.org/en/latest/installation.html) >= 1.4.0: This can be installed with pip:

  ```bash
  pip install cookiecutter
  ```

## Quickstart

### Create a new project

Just run:

``` bash
cookiecutter https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package.git  --checkout dev-loic
```

(_this should be run from the location that you want the project folder to live, or you will need to move the directory around later._)

If you have previously created a package with this template confirm the prompt to redownload the newest version.
The installation dialog will ask for a few inputs:

- `project_name`: The name of the project. **If you want to associate this project with a gitlab repository, this should be the same name as the repository name on gitlab for consistency (e.g. my_awesome_project).
- `author_name`: Your name / your company name.
- `description`: A short description of the project for the readme.
- `license`: Chose a license for your package. Currently available licenses are: "MIT" and "BSD-3-Clause", details can be found [here](https://opensource.org/licenses/).

> Unfortunately there seems to be a bug that does [not allow backspace](https://github.com/audreyr/cookiecutter/issues/875) in cookiecutter on certain platforms. If you make a typo cancel the input `ctrl+c` and start over again.

### The resulting directory structure

The directory structure of your new project looks like this:

```shell
│
├── README.md         <- The top-level README for developers using this project.
├── Makefile          <- Makefile with commands like `make format` or `make check`,
│                         additional conda package may have to be installed (e.g. flake8)
├── requirements.txt  <- pip package requirement file
├── setup.py          <- makes project pip installable (pip install -e .)
│                         so source code can be imported    
├── docs              <- A default Sphinx project; see README.md and sphinx-doc.org for details
├── src               <- Source code for use in this project
│   └── project_name  <- Name of the pip package
│       ├── __init__.py   <- Makes project_name a Python module
│       ├── dummy_module.py
│       └── subpackage_test
│           └── __init__.py
└── tests              <- Directory for test functions
    └── test_dummy.py
```

------------

### Setting up a GitLab repository

[Create a new project on GitLab](https://gitlab.com/projects/new#blank_project), using the same `project_name`. Don't choose _initialize repository with a README_.

Then on the next page, follow the instructions to **Create a new repository**. In a terminal, just do:

```bash
cd existing_folder
git init
git remote add origin https://gitlab.com/[username]/[project_name].git
git add .
git commit -m "Initial commit"
git push -u origin main
```

### More

More information on how to use cookiecutter template can be found [here](https://cookiecutter.readthedocs.io/en/stable/)
