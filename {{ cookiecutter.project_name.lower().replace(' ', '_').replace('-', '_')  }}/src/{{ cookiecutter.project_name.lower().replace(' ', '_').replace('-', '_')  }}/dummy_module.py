"""
Docstring for the dummy_module.py module.
"""


def dummy_foo(num):
    """
    Dummy function for testing purpose.

    Parameters
    ----------
    num: float
        a number

    Returns
    -------
    float
        the sum of a with 4

    Examples
    --------
    >>> from test_package_py39.dummy_module import dummy_foo
    >>> test_num = dummy_foo(0)
    >>> print(test_num)
    4
    """
    return num + 4
