# {{cookiecutter.project_name}}

{{cookiecutter.description}}

## Getting Started

### Install the package

#### Local package

In the project root directory, run:

```shell
pip install .
```

#### From package repository

```shell
pip install {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}} 
```

### Package structure

```shell
│
├── README.md         <- The top-level README for developers using this project.
├── Makefile          <- Makefile with commands such as `make format` or `make check`,
├── docs              <- A default Sphinx project; see README.md and sphinx-doc.org for details
│   ├── Makefile
│   ├── README.md
│   └── source
│       ├── conf.py
│       ├── index.rst
│       ...
│      
├── pyproject.toml    <- package project settings file
├── setup.cfg         <- package project configuration file
├── setup.py          <- package file for legacy compatibility
├── src               <- Source code for use in this project
│   └── {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}   <- Name of the pip package
│       ├── __init__.py   <- Makes {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}  a Python module
│       ├── dummy_module.py
│       └── subpackage_test
│           └── __init__.py
└── tests              <- Directory for test functions
    └── test_dummy.py
```

---

## Development

### Create a virtual environment

If you have installed `pyenv` and `pyenv-virtualenv` following [instructions on IAG confluence](https://dwpdigital.atlassian.net/wiki/spaces/IASC/pages/133084382758/Python+-+versions+virtual+environments):

```shell
# Install Python 3.9
pyenv install 3.9
```

```shell
# Create a virtualenvironement based on python 3.9 named my-venv-3.9
pyenv virtualenv 3.9 my-venv-3.9
```

### Activate a virtual environment

#### Manually

```shell
pyenv activate my-venv-3.9
```

#### Or automatically when entering the current directory

```shell
pyenv local my-venv-3.9
```

### Install the editable package version

The `-e` argument has to be used to be able to see changes of the code source in
"real-time". In the project root directory, run:

```shell
pip install -e .  
```

### Install optional development packages

```shell
pip install -e .[dev] 
```

or on `zsh` bracket characters need to be escaped:

```shell
pip install -e .\[dev\] 
```

### Run the "tests"

```shell
pytest tests  
```

### Documentation

To build the documentation, open the [`docs/README.md`](docs/README.md)

To serve the doc on GitLab Pages using GitLab CI pipelines,
make sure GitLab Pages is activated in the repository settings
(Settings/General/Visibility).
Once the CI pipeline has finished the library documentation should be accessible
at <https://dwp.gitlab.io/intelligent-automation-garage/...>

### Code style

Coding standards followed by the team are detailed on
[IAG Confluence Page (c.f. link to Python Coding Standard)](https://dwpdigital.atlassian.net/wiki/spaces/IASC/pages/70717637748/Coding+Standards).
To format and check the code style, the following tools are used:

* [Black](https://github.com/psf/black)
* [blackdoc](https://github.com/keewis/blackdoc)
* [docformatter](https://github.com/myint/docformatter)
* [isort](https://pycqa.github.io/isort/)

Black and blackdoc loosely follows the [PEP8](http://pep8.org) guide but
with a few differences. Before committing, the formatting can be run
automatically with:

```bash
make format
```

This project also uses [flake8](http://flake8.pycqa.org/en/latest/) and
[pylint](https://www.pylint.org/) to check the quality of the code and quickly catch
common errors.
The [`Makefile`](Makefile) contains rules for running both checks:

```bash
make check   # Runs black, blackdoc, docformatter, flake8 and isort (in check mode)
make lint    # Runs pylint, which is a bit slower
```

#### Docstrings

**All docstrings** should follow the
[numpy style guide](https://numpydoc.readthedocs.io/en/latest/format.html#docstring-standard).
All functions/classes/methods should have docstrings with a full description of all
arguments and return values.

The maximum line length for code is automatically set by *Black* and the docstrings
by *blackdoc*. To play nicely with Jupyter and IPython, **keep docstrings
limited to 119 characters** per line.

### Pre-Commit

To install and run pre-commit hooks used at DWP, read the [relevant documentaton](docs/pre-commit-config.README.md).

### CI/CD

Python specific CI/CD function and fragments can be found here:

* [python function](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/functions/-/blob/main/functions/python.README.md)

* [python-tests-with-pytest](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/fragments/python-tests-with-pytest)

* [python-code-format-check](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/fragments/python-code-format-check)

* [python-analysis-with-pylint](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/fragments/python-analysis-with-pylint)

* [python-doc-building-with-sphinx](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/fragments/python-doc-building-with-sphinx)

An issue is opened to integrate the fragment above in the python pipeline function ([see issue](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/functions/-/issues/70))

In addition, the [Auto-Tag Merge Function](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/functions/-/blob/main/functions/auto-tag-merge.README.md) is also used for automate versioning based commit types (see [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version)).

### Local package build (should not be needed as already done in `.gitlab-ci.yml`)

To build the package locally:

```shell
 python setup.py sdist bdist_wheel
```

To publish the package into GitLab package registry you can run the command:

```shell
TWINE_PASSWORD=<personal_access_token or deploy_token> TWINE_USERNAME=<username or deploy_token_username> python3 -m twine upload --repository-url https://gitlab.example.com/api/v4/projects/<project_id>/packages/pypi dist/*
```

More info [[here](https://docs.gitlab.com/ee/user/packages/pypi_repository/#publish-a-pypi-package)]

### Install pypi package in another project

First, create an access token for the current repository (`Settings/Access Tokens`) with `read_api` and `read_registry` scope.

Then two different ways are possible to import pip package in another project:

* Method 1: Add the repository URL in the `requirement.txt` file of the other project:

```text
git+https://<access_token_name>:<acces_token>@gitlab.com/dwp/intelligent-automation-garage/python-packages/{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}.git@0.0.1
```

* Method 2: Create a `PIP_EXTRA_INDEX_URL` pointing toward the package registry of the repository:

```text
https://<access_token_name>:<acces_token>@gitlab.com/api/v4/projects/<project-id>/packages/pypi/simple
```

If used in CI/CD job in a docker build, the option `--build-arg` may be needed.

---
*Project based on a [cookiecutter project template](https://gitlab.com/dwp/intelligent-automation-garage/templates/pip-package).*
