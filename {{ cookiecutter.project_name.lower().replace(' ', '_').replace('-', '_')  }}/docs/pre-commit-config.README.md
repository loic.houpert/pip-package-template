# Pre-Commit

_original readme file can be found [here](https://gitlab.com/dwp/engineering/pipeline-solutions/gitlab/functions/-/blob/main/documentation/.pre-commit-config.README.md)_

We use [pre-commit](https://pre-commit.com/) to supply a language agnostic way
of running pre-commit <!-- alex disable hooks-->hooks<!-- alex enable hooks-->
within
pipeline-solutions functions. This lightweight config file is designed with
compatibility to other DWP projects in mind, so you can
install pre-commit on your local machine and copy the `.pre-commit-config.yaml`
at the root level of your repo.

Ensuring you conform to this standard is vital for uniformity between DWP
Projects,
but also will ensure:

- you can capture changes in the CHANGELOG.md automatically when
  running `npx --yes commit-and-tag-version` to create new versions of your app ('--yes' bypasses being asked to install)
- secrets are never committed into GitLab
- line-endings are consistent
- and much more Quality of Life improvements when moving between repos

## Pre-Commit Rules

The pre-commit check is run on a `git commit`

## Available Example pre-commit-configs

1. `.example-Js-pre-commit-config`: uses eslint with the dwp/eslint-config-base ruleset to format Js and Ts files, with Prettier used for all other files. **Requires** an Eslint config to be present in the repo
2. `.example-pre-commit-config`: uses Prettier to format all files, is incompatible with `node-eslint` job, as Prettier produces a differing code-style to that preferred by `dwp/eslint-config-base` rules

## Installing and Using [Pre-Commit](https://pre-commit.com/#installation)

We recommend using Pip to install pre-commit on your device (for consistency
between UC Macbooks and other DWP Macbooks).

1. Install Pip and Python using the Self-Service tool

1. Install pre-commit (at least v1.18) using pip: `pip install pre-commit` or
   brew `brew install pre-commit` (upgrade with `brew upgrade pre-commit`)

1. If you are using the example config you will need to install
   pre-commit-config **dependencies**
1. `shellcheck`, [`ruby@1.0`](https://mac.install.guide/ruby/11.html)
   , `semgrep`, `golang`

1. Install Node-Version-Manager (nvm) and Node long-term-support release (
   alternatively you can install a singular version of Node using the
   Self-Service tool)

1. [Installing NVM](https://github.com/nvm-sh/nvm#installing-and-updating):

```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```

1. Add the following to `~/.zshrc` or desired shell

```shell
# Load nvm
export NVM_DIR="$HOME/.nvm"
[ -s "/Users/tim.knight1/.homebrew/opt/nvm/nvm.sh" ] \
  && \. "/Users/tim.knight1/.homebrew/opt/nvm/nvm.sh"

# This loads nvm bash_completion
[ -s "/Users/tim.knight1/.homebrew/opt/nvm/etc/bash_completion.d/nvm" ] \
  && \. "/Users/tim.knight1/.homebrew/opt/nvm/etc/bash_completion.d/nvm"
```

1. `source .~/.zshrc`
1. Install the LTS of node `nvm install --lts`

1. Copy the following files from this project into your own:

1. `.pre-commit-config.yaml`
1. `.pre-commit-config.README.md` (so new contributors understand how to
   setup pre-commit in your repo) into the root of your repo:
1. Copy the `commitlint.config.js` at the root of your project

1. Follow these steps to install pre-commit (using these steps rather then
   pre-commit install should mean it will automatically enable checks on repos
   which define a pre-commit-config.yaml, but will not fail on those which do
   not yet)

1. [Full steps and Examples](https://pre-commit.com/#automatically-enabling-pre-commit-on-repositories)
1. **TLDR** run the following terminal commands:
1. `git config --global init.templateDir ~/.git-template`
1. `pre-commit init-templatedir ~/.git-template`
1. Now whenever you clone a pre-commit enabled repo,
   the <!-- alex disable hooks-->hooks<!-- alex enable hooks--> will already
   be set up!
1. If you have already cloned a repo and wish to add pre-commit to it,
   you will need to run `pre-commit install --install-hooks` within that
   repo

1. Verify the config by running: `pre-commit run --all-files`, this will do a
   one-off check against all files in the repo
1. Ensure you are signed into zScaler

## Additional installs (for tools run by pre-commit config)

1. Golang
2. Python + Pip
3. shfmt - `brew install shfmt`
4. trufflehog `brew tap trufflesecurity/trufflehog`, then `brew install trufflehog`

## Configuring [Alex](https://github.com/get-alex/alex) (checks MD content)

### Case-by-case config

You can configure [Alex](https://github.com/get-alex/alex) on case-by-case
basis, for example to allow through words such
as "pom", as in "pom.xml", which are flagged as a profane (and as alex is not
context-aware it cannot reconcile the correct use of the word in maven from the
insult).

By adding the following around the specific word you wish to
exclude `<!-- alex disable pom-->pom<!-- alex enable pom-->`

### Global config

Add a `.alexrc` file to the repo with config such as:

```json
{
  "allow": ["just", "husky"],
  "ignore": "CHANGELOG.md"
}
```

## Configuring [Codespell](https://github.com/codespell-project/codespell)

You can define a `.codespellrc` file with config such as

```yaml
[codespell]
ignore-words-list = happywitthisword
skip = package-lock.json,CHANGELOG.md
```

## Configuring [Shellcheck](https://www.shellcheck.net/)

This config should be picked up the shell-check job as well as the pre-commit <!-- alex disable hooks-->hooks<!-- alex enable hooks-->.

You can define a `.shellcheckrc` file with config such as:

```yaml
disable=SC3003,SC3030,SC3010,SC3046,SC1090,SC2199,SC1083,SC3054 # individual error codes
disable=SC1090-SC1100 # Disable a range of error codes
```

### Tools Used

1. [pre-commit](https://pre-commit.com/)
1. [commitlint/cli](https://www.npmjs.com/package/@commitlint/cli)
1. [@dwp/commitlint-config-base](https://github.com/dwp/commitlint-config-base)
1. [commitlint-pre-commit-hook](https://github.com/alessandrojcm/commitlint-pre-commit-hook)
1. [alex](https://github.com/get-alex/alex)
