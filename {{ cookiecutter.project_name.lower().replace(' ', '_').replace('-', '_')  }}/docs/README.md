# Generate a local version of the documentation

## Install optional packages to build the doc

```shell
pip install -e ..[doc] 
```

or on `zsh` bracket characters need to be escaped:

```shell
pip install -e ..\[doc\] 
```

## Build the doc

```shell
make clean
```

### html format

```shell
make html
```

### PDF format

```shell
make latexpdf
```

To see all the different options run `make` in the `docs` folder
