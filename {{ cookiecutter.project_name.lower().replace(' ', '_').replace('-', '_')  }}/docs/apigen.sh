#!/bin/sh
echo " "
echo "~~~~~~~~~~~~~~~~~~~ "
echo "Building API docs."
# script to automatically generate the  {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}} api documentation using
# sphinx-apidoc and sed
sphinx-apidoc -f -M -e  -T -o source/api ../src/{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}/
# replace chapter title in sphinx_nbexamples.rst
sed -i -e 1,1s/.*/'API Reference'/ source/api/{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}.rst
sed -i -e 2,2s/.*/'============='/ source/api/{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}.rst
echo "~~~~~~~~~~~~~~~~~~~ "
