import pytest
from {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}.dummy_module import dummy_foo


def test_dummy_returns_correct_result():
    assert dummy_foo(4) == (4 + 4)


def test2_dummy_raises_TypeError_if_input_is_string():
    with pytest.raises(TypeError):
        assert dummy_foo("foo")
